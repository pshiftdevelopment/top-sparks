<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Email;

class MailController extends Controller
{


	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{

	}

	/**
	* Show the application dashboard.
	*
	* @return \Illuminate\Contracts\Support\Renderable
	*/
	public function sendmail(Request $request)
	{
		$message = $request->message;

		// Don't send if contains a link
		if (strpos($message, 'http://') !== false || strpos($message, 'https://') !== false) {
			return redirect('/contact-us/');
		}
		
		$to = explode(',', config('mail.to'));
		Mail::to($to)->send(new Email($request->all()));

		// return redirect('/thank-you/');
		return redirect('/contact-us/')->with('success', 'Thank you for your enquiry');
		// return view('contact-us')->with('success', 'Thank you for your enquiry');
	}

}
