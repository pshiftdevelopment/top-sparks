<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('home', function () {
    return view('home');
});

Route::get('contact-us', function () {
    return view('contact-us');
});

Route::get('services', function () {
    return view('services.index');
});

Route::get('services/design', function () {
    return view('services.design');
});

Route::get('services/installation', function () {
    return view('services.installation');
});

Route::get('services/maintenance', function () {
    return view('services.maintenance');
});

Route::get('services/energy-conservation', function () {
    return view('services.energy-conservation');
});

Route::get('services/property-block-management-services', function () {
    return view('services.property-block-management-services');
});

Route::get('services/security', function () {
    return view('services.security');
});

Route::get('services/cctv', function () {
    return view('services.cctv');
});

Route::get('about-us', function () {
    return view('about-us.index');
});
Route::get('about-us/about-us', function () {
    return view('about-us.index');
});

Route::get('about-us/domestic-work', function () {
    return view('about-us.domestic-work');
});

Route::get('about-us/commercial-work', function () {
    return view('about-us.commercial-work');
});

Route::get('services/testing-and-inspection', function () {
    return view('services.testing-and-inspection');
});

Route::get('privacy', function () {
    return view('privacy');
});

Route::get('terms-and-conditions', function () {
    return view('terms');
});

Route::get('case-studies', function () {
    return view('case-studies');
});

Route::get('case-studies/our-grand-designs-project', function () {
    return view('case-studies.our-grand-designs-project');
});

Route::get('case-studies/house-re-wire-mirfield', function () {
    return view('case-studies.house-re-wire-mirfield');
});

Route::get('case-studies/3-phase-machine-installation', function () {
    return view('case-studies.3-phase-machine-installation');
});

Route::get('case-studies/warehouse-lighting-upgrade', function () {
    return view('case-studies.warehouse-lighting-upgrade');
});

Route::get('case-studies/shop-rewire-leeds', function () {
    return view('case-studies.shop-rewire-leeds');
});

Route::get('case-studies/office-refurbishment-cat-5-and-power-installation', function () {
    return view('case-studies.office-refurbishment-cat-5-and-power-installation');
});

Route::get('case-studies/intruder-alarm-installation', function () {
    return view('case-studies.intruder-alarm-installation');
});



Route::post('contact-us', 'MailController@sendmail');
