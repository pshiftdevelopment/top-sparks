jQuery(document).ready(function(){

	jQuery('#resi-nav').click(function (){
		jQuery('.navigation#top').toggleClass('open');
	});
	
	function checkWidth() {
		
		var $window = $(window);
        var windowsize = $window.width();

        if (windowsize < 1000) {
            
            jQuery('ul li a.drop-trig').click(function() {
	          $(this).next().toggleClass('open-sub');
			  $(this).data('href', $(this).attr('href'));
			  $(this).removeAttr('href');
			  $(this).preventDefault();
			});
            
        } else {
	        jQuery('ul li a.drop-trig').click(function() {
		    	 $(this).attr('href', $(this).data('href'));
			});
        }
    }
    // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);
    
    // WRAP IN DOC READY 
	jQuery('.c-agree').click(function() {
	    
	    setCookie("gdpr_popup", "true", 3);
	    jQuery('.cookie-notice').hide();
	    
	});
	  
	confirm_cookie = getCookie("gdpr_popup");
	  
	if (confirm_cookie == null) {
		jQuery('.cookie-notice').show();
	}
	// WRAP IN DOC READY END
	
	function setCookie(c_name, value, exdays) {
	    var exdate = new Date();
	    exdate.setDate(exdate.getDate() + exdays);
	    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	    document.cookie = c_name + "=" + c_value + ";path=/";
	}
	
	function getCookie(c_name) {
	    var i, x, y, ARRcookies = document.cookie.split(";");
	    for (i = 0; i < ARRcookies.length; i++) {
	        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
	        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
	        x = x.replace(/^\s+|\s+$/g, "");
	        if (x == c_name) {
	            return unescape(y);
	        }
	    }
	}

    
});