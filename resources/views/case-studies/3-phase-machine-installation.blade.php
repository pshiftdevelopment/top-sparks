
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Case Study Grand Design Project | Top Sparks</title>
    <meta name="keywords" content="electricians in bradford, electricians in leeds, electricians in yorkshire, electrical contractors, electrical electricians, electrical services" />
    <meta name="description" content="Specialist CHAS approved electrical contractors for commercial, industrial &amp; domestic electrical work in Bradford &amp; Leeds. Free no obligation quote or site visit." />
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>
  </head>
  <body id="case-studies">
    <div class="header">
      <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class=" first">
            <a class="" href="/home/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="active">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <h2 class="case-study-index">Case Studies Index</h2>
        <ul class="navigation" id="side-case-studies">
          <li>
            <span class="">Design</span>
            <ul>
              <a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project"><span>›</span>&nbsp;Our 'grand designs' project</a>
            </ul>
          </li>
          <li class="active">
            <span class="">Installation</span>
            <ul style="display: block;">
              <a href="/case-studies/house-re-wire-mirfield" title="House re-wire Mirfield" ><span>›</span>&nbsp;House re-wire Mirfield</a>
              <a class="active" href="/case-studies/3-phase-machine-installation" title="3 Phase Machine Installation"><span>›</span>&nbsp;3 Phase Machine Installation</a>
              <a href="/case-studies/warehouse-lighting-upgrade" title="Warehouse lighting upgrade"><span>›</span>&nbsp;Warehouse lighting upgrade</a>
              <a href="/case-studies/shop-rewire-leeds" title="Shop rewire Leeds"><span>›</span>&nbsp;Shop rewire Leeds</a>
              <a href="/case-studies/office-refurbishment-cat-5-and-power-installation" title="Office refurbishment - Cat 5 and power installation"><span>›</span>&nbsp;Office refurbishment - Cat 5 and power installation</a>
            </ul>
          </li>
          <li>
            <span class="">Security</span>
            <ul style="display: none;">
              <a href="/case-studies/intruder-alarm-installation" title="Intruder Alarm installation"><span>›</span>&nbsp;Intruder Alarm installation</a>
            </ul>
          </li>
        </ul>
        <div class="text contact-us">
          <p>Want to know more about the services we provide?<span>Get in contact</span></p>
          <a href="/contact-us" title="Contact Us" class="button">› Contact us</a>
        </div>
      </div>
      <div class="column2-right">

        <div class="text main"><h1>The installation of 3 phase sub-board.</h1>
        	<p>The installation was completed within budget and included the following :-</p>
<ul>
<li>3-Phase installation</li>
<li>Machine installation</li>
</ul>


		</div>
       
      </div>
      <span class="clear"></span>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
