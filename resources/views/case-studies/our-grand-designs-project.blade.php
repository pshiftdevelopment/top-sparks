
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Case Study Grand Design Project | Top Sparks</title>
    <meta name="keywords" content="electricians in bradford, electricians in leeds, electricians in yorkshire, electrical contractors, electrical electricians, electrical services" />
    <meta name="description" content="Specialist CHAS approved electrical contractors for commercial, industrial &amp; domestic electrical work in Bradford &amp; Leeds. Free no obligation quote or site visit." />
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>
  </head>
  <body id="case-studies">
    <div class="header">
      <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="first">
            <a class="" href="/home/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="active">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <h2 class="case-study-index">Case Studies Index</h2>
        <ul class="navigation" id="side-case-studies">
          <li class="active">
            <span class="">Design</span>
            <ul>
              <a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project" class="active"><span>›</span>&nbsp;Our 'grand designs' project</a>
            </ul>
          </li>
          <li>
            <span class="">Installation</span>
            <ul style="display: none;">
              <a href="/case-studies/house-re-wire-mirfield" title="House re-wire Mirfield"><span>›</span>&nbsp;House re-wire Mirfield</a>
              <a href="/case-studies/house-extension-harrogate" title="House extension- Harrogate"><span>›</span>&nbsp;House extension - Harrogate</a>
              <a href="/case-studies/3-phase-machine-installation" title="3 Phase Machine Installation"><span>›</span>&nbsp;3 Phase Machine Installation</a>
              <a href="/case-studies/warehouse-lighting-upgrade" title="Warehouse lighting upgrade"><span>›</span>&nbsp;Warehouse lighting upgrade</a>
              <a href="/case-studies/shop-rewire-leeds" title="Shop rewire Leeds"><span>›</span>&nbsp;Shop rewire Leeds</a>
              <a href="/case-studies/office-refurbishment-cat-5-and-power-installation" title="Office refurbishment - Cat 5 and power installation"><span>›</span>&nbsp;Office refurbishment - Cat 5 and power installation</a>
            </ul>
          </li>
          <li>
            <span class="">Security</span>
            <ul style="display: none;">
              <a href="/case-studies/intruder-alarm-installation" title="Intruder Alarm installation"><span>›</span>&nbsp;Intruder Alarm installation</a>
            </ul>
          </li>
        </ul>
        <div class="text contact-us">
          <p>Want to know more about the services we provide?<span>Get in contact</span></p>
          <a href="/contact-us" title="Contact Us" class="button">› Contact us</a>
        </div>
      </div>
      <div class="column2-right">
        <div class="gallery">
			<img src="{{ asset('/uploads/images/photo-16-4fb7a41bb0388.jpg') }}" alt="">
        </div>
        <ul class="thumbnails">

          <li class="first active">
            <a href="{{ asset('/uploads/images/photo-16-4fb7a41bb0388.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-16-4fb7a41bb0388.jpg') }}" alt="">
            </a>
          </li>
          <li>
            <a href="{{ asset('/uploads/images/photo-15-4fb7a42a66bb1.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-15-4fb7a42a66bb1.jpg') }}" alt="">
            </a>
          </li>
          <li>
            <a href="{{ asset('/uploads/images/photo-14-4fb7a43d7fe7c.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-14-4fb7a43d7fe7c.jpg') }}" alt="">
            </a>
          </li>
          <li>
            <a href="{{ asset('/uploads/images/photo-13-4fb7a453f131c.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-13-4fb7a453f131c.jpg') }}" alt="">
            </a>
          </li>
          <li>
            <a href="{{ asset('/uploads/images/photo-12-4fb7a46e0d6af.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-12-4fb7a46e0d6af.jpg') }}" alt="">
            </a>
          </li>
          <li>
            <a href="{{ asset('/uploads/images/photo-11-4fb7a47e7e35f.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-11-4fb7a47e7e35f.jpg') }}" alt="">
            </a>
          </li>
          <li>
            <a href="{{ asset('/uploads/images/photo-10-4fb7a4aa35b6e.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-10-4fb7a4aa35b6e.jpg') }}" alt="">
            </a>
          </li>
          <li>
            <a href="{{ asset('/uploads/images/photo-9-4fb7a4c2a15fb.jpg') }}" title="">
              <img src="{{ asset('/uploads/thumbs/photo-9-4fb7a4c2a15fb.jpg') }}" alt="">
            </a>
          </li>
        </ul>
        <div class="text main"><h1>Our 'grand designs' project</h1><p>A local property developer was looking to build his own designer property.&nbsp; Within the shell of victorian mill, this 'grand designs' style residence proved to be one of the most challenging and complex projects we've tackled to date.</p>
<p>However the results speak for themselves! Working to the highest levels of specification and level of detail every aspect of this project was completed to our clients complete satisfaction. The build used many different technologies including:</p>
<ul>
<li>LED lighting</li>
<li>Intruder alarm system</li>
<li>CCTV system</li>
<li>Intercom/ door access system</li>
<li>TV/ HD/SKY distribution system</li>
<li>BOSE HiFi installation.</li>
<li>General power and lighting</li>
<li>CAT 5 network cabling</li>
<li>Telephone system</li>
<li>Computer-controlled underfloor heating system</li>
</ul>
</div>
        <div class="related-case-studies">
          <h2 class="h1">Related Case Studies</h2>
          <li class="first">
            <a href="/case-studies/new-build-house" title="New Build House" class="thumbnail">
              <img src="{{ asset('/uploads/images/293669_10150273528958008_31859-4f9bfecfe431c.jpg') }}" alt="New Build House">
            </a>
            <h2>
              <a href="/case-studies/new-build-house" title="New Build House">New Build House</a>
            </h2>
            <a href="/case-studies/new-build-house" class="more">› Read Case Study</a>
          </li>
        </div>
      </div>
      <span class="clear"></span>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
