@section('footer')

<!--=============footer start here ================-->
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #aebac2;" bgcolor="#aebac2">
			<tr>
				<td height="25" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td>
			</tr>
			<tr>
				<td style="text-align: center; color: #49555f; font-size: 12px; line-height: 20px; mso-line-height-rule: exactly; letter-spacing: 0.02em; font-family: Arial, sans-serif;">
					Copyright © @php echo date('Y') @endphp {{ config('app.name') }}
				</td>
			</tr>
			<tr>
				<td height="25" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td>
			</tr>
		</table>
	</td>
</tr>
<!--=============footer end here ================-->

<!-- ************thats all folks************ -->
			</table>
	</td>
  </tr>
  	<tr><td height="10" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td></tr>
</table>
</body>
</html>

@stop
