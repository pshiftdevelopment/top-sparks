@include('mail.header')
@include('mail.footer')


@yield('header')

<!--=============banner and section 1 start here ================-->
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d0dae4" style="background-color: #d0dae4;">
			<tr><td height="25" colspan="3" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td></tr>
			<tr>
				<td width="25"></td>
				<td width="550" style="text-align: center; font-size: 18px; line-height: 22px; mso-line-height-rule: exactly; letter-spacing: 0.04em; color: #0c3ba5; font-family: Arial, sans-serif; text-transform: uppercase;">
					{{ config('app.name') }} Website Notificaton
				</td>
				<td width="25"></td>
			</tr>
			<tr><td height="25" colspan="3" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="background-color: #ffffff;">
			<tr><td height="44" colspan="3" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td></tr>
			<tr>
				<td width="64"></td>
				<td width="472" style="font-size: 14px; line-height: 22px; mso-line-height-rule: exactly; letter-spacing: 0.02em; color: #49555f; font-family: Arial, sans-serif;">

					An enquiry has been made from the website:<br/><br/>

					<b>Name</b> - {{ $userdata['name'] }}<br/><br/>
					<b>Email</b> - <a href="mailto:{{ $userdata['email'] }}" style="color: #ec6f2d; font-size: 14px; line-height: 22px; mso-line-height-rule: exactly; letter-spacing: 0.02em; font-family: Arial, sans-serif;">{{ $userdata['email'] }}</a><br/><br/>
					<b>Message</b><br/>
					{{ $userdata['message'] }}

				</td>
				<td width="64"></td>
			</tr>
			<tr><td height="44" colspan="3" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td></tr>
		</table>
	</td>
</tr>
<!--=============banner and section 1 end here ================-->

@yield('footer')
