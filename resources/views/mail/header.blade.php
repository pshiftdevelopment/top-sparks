@section('header')

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="format-detection" content="date=no">
<meta name="format-detection" content="telephone=no">
<title>{{ config('app.name') }} Website Notification</title>
<style type="text/css">
/*Hotmail and Yahoo specific code*/
.ReadMsgBody {
	width: 100%;
}
.ExternalClass {
	width: 100%;
}
/*Hotmail and Yahoo specific code*/
body {
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
	-webkit-font-smoothing: antialiased;
	margin: 0 !important;
	padding: 0 !important;
	width: 100% !important;
}
img { display: block; }
a[href^="x-apple-data-detectors:"] {
color: inherit;
text-decoration: inherit;
}
</style>
</head>
<body style="margin:0; padding:0; background-color:#ebebeb;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; background-color:#ebebeb;" bgcolor="#ebebeb">
	<tr><td height="10" style="font-size: 0; line-height: 0px; mso-line-height-rule: exactly;"></td></tr>
  <tr>
    <td align="center" valign="top" >
			<table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="background-color: #ffffff;">

<!-- *********lets go**************** -->
<!--=============header start here ================-->
<tr>
	<td colspan="3" width="600" height="120" valign="center" align="center">
		<a href="{{url('/')}}" target="_blank">
			<img src="{{ asset('images/email-header.png') }}" alt="{{ config('app.name') }} - Logo" width="372" height="84" style="border: 0; display: block;">
		</a>
	</td>
</tr>
<!--=============header end here ================-->

@stop
