
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Contact Us | Top Sparks</title>
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>
  </head>
  <body id="contact-us">
    <div class="header">
      <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="active first">
            <a class="" href="/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <div class="text main"><h1>Contact Us</h1><p style="color: #333; font-size: 18px;"><strong>Phone:</strong> 07958 521227<br />
<br />
<strong>Email:</strong> nick.scanlon@topsparks.co.uk</p>
<p style="color: #333; font-size: 18px;"><strong>Twitter:</strong> <a href="https://twitter.com/top_sparks" target="_blank">@top_sparks</a></p>
<p>Top Sparks Ltd<br />
22 Portal Crescent,<br />
Mirfield<br />
WF14 0JJ</p>
</div>
      </div>
      <div class="column2-right">
        <div class="text main">
          <h1>Enquiry Form</h1>
        </div>

        <form action="/contact-us" method="POST">

			  @csrf

          <fieldset>
            <label>Name <input type="text" name="name" value="" class="input-text" /></label>
            <label>Email <input type="email" name="email" value="" class="input-text" /></label>
            <label>Telephone <input type="text" name="phone" value="" class="input-text" /></label>
            <label>Message <textarea name="message" rows="5" cols="21" class="input-text"></textarea></label>

            {{-- <input name="send-email[sender-email]" value="fields[email]" type="hidden" />
            <input name="send-email[sender-name]" value="fields[name]" type="hidden" />
            <input name="send-email[reply-to-email]" value="fields[email]" type="hidden" />
            <input name="send-email[reply-to-name]" value="fields[name]" type="hidden" />
            <input name="send-email[subject]" value="You are being contacted" type="hidden" />
            <input name="send-email[body]" value="fields[name],fields[email],fields[phone],fields[message]" type="hidden" />
            <input name="send-email[recipient]" value="topsparks" type="hidden" /> --}}
            <input id="submit" type="submit" name="action[enquiry]" value="Send Enquiry" class="input-submit c4" />
          </fieldset>
        </form>

		  @if(session()->get('success'))
			  <div class="alert alert-success">
				  {{ session()->get('success') }}
			  </div>
		  @endif
      </div>
      <span class="clear"></span>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
