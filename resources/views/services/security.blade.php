
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Security Installations | Top Sparks</title>
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>

  </head>
  <body id="services">
    <div class="header">
     <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="first">
            <a class="" href="/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown active">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <ul class="navigation" id="side-services">
          <li class="first">
            <a href="/services/design" title="Design">
              <span>Design</span>
            </a>
          </li>
          <li class="">
            <a href="/services/installation" title="Installation">
              <span>Installation</span>
            </a>
          </li>
          <li class="">
            <a href="/services/maintenance" title="Maintenance">
              <span>Maintenance</span>
            </a>
          </li>
          <li class="">
            <a href="/services/energy-conservation" title="Energy Conservation">
              <span>Energy Conservation</span>
            </a>
          </li>
          <li class="">
            <a href="/services/property-block-management-services" title="Block Management Services and Maintenance Contracts">
              <span>Property Management</span>
            </a>
          </li>
          <li class=" active">
            <a href="/services/security" title="Security">
              <span>Security</span>
            </a>
          </li>
          <li class="">
            <a href="/services/cctv" title="Closed Circuit Television CCTV">
              <span>CCTV</span>
            </a>
          </li>
          <li class="last">
            <a href="/services/testing-and-inspection" title="Testing and Inspection">
              <span>Testing and Inspection</span>
            </a>
          </li>
        </ul>
        <div class="text contact-us">
          <p>Want to know more about this service?<span>Get in contact</span></p>
          <a href="/contact-us" title="Contact Us" class="button">› Contact us</a>
        </div>
      </div>
      <div class="column2-right">
        <ul class="gallery cycle" style="position: relative;">
          <li style="position: absolute; top: 0px; z-index: 2; opacity: 0; display: none;">
            <img src="{{ asset('/uploads/gallery/security1-59b5168260de7.jpg') }}" alt="">
          </li>
          <li style="position: absolute; top: 0px; z-index: 1; display: block; opacity: 1;">
            <img src="{{ asset('uploads/gallery/security2-59b516e6bb51e.jpg') }}" alt="">
          </li>
        </ul>
        <div class="text main"><h1>Security</h1><p>Our experience with intruder alarm systems goes back many years. Our MD, Nick Scanlon worked in the Crime Prevention Department of West Yorkshire Police for 10 years. With this background in security, we can offer practical advice&nbsp;and specify a system to secure your premises and put your mind at rest.&nbsp;</p>
<ul>
<li>Intruder alarm systems</li>
<li>Monitored systems will dial or text you in the event of an alarm activation.</li>
<li>Security lighting</li>
<li>Pet-tolerant systems</li>
<li>Access control systems</li>
<li>Intercoms</li>
<li>Covert systems</li>
</ul>
</div>
        <div class="related-case-studies">
          <h2 class="h1">Related Case Studies</h2>
          <li class="first">
            <a href="/case-studies/intruder-alarm-installation" title="Intruder Alarm installation" class="thumbnail">
              <img src="{{ asset('/uploads/thumbs/25115_358345503007_672343007_3-4f9bfc59cf147.jpg') }}" alt="Intruder Alarm installation">
            </a>
            <h2>
              <a href="/case-studies/intruder-alarm-installation" title="Intruder Alarm installation">Intruder Alarm installation</a>
            </h2>
            <a href="/case-studies/intruder-alarm-installation" class="more">› More information</a>
          </li>
        </div>
      </div>
      <span class="clear"></span>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
