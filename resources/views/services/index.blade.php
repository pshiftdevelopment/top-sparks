
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Services | Top Sparks</title>
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>

  </head>
  <body id="services">
    <div class="header">
     <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="first">
            <a class="" href="/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown active">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="text single"><h1>Our Services</h1><p>We offer a full range of electrical services from the design and installation of new electrical systems through to the testing and maintenance of existing electrical installations.</p>
</div>
      <ul class="service-list">
        <li class="first top">
          <a href="/services/design" title="Design" class="thumbnail">
            <img src="{{ asset('/uploads/images/design-4fa13c68cf557.jpg') }}" alt="Design" />
          </a>
          <h2>
            <a href="/services/design" title="Design">Design</a>
          </h2>
          <a href="/services/design" class="more">› More information</a>
        </li>
        <li class=" top">
          <a href="/services/installation" title="Installation" class="thumbnail">
            <img src="{{ asset('/uploads/images/installation-4fa139a9b1685.jpg') }}" alt="Installation" />
          </a>
          <h2>
            <a href="/services/installation" title="Installation">Installation</a>
          </h2>
          <p class="description">We carry out high quality installations in al...
        </p>
          <a href="/services/installation" class="more">› More information</a>
        </li>
        <li class=" top">
          <a href="/services/maintenance" title="Maintenance" class="thumbnail">
            <img src="{{ asset('/uploads/images/installation-t-4f8e83fbe3e68-4f9fe9e2e3046.jpg') }}" alt="Maintenance" />
          </a>
          <h2>
            <a href="/services/maintenance" title="Maintenance">Maintenance</a>
          </h2>
          <a href="/services/maintenance" class="more">› More information</a>
        </li>
        <li class="clear"></li>
        <li class="first">
          <a href="/services/energy-conservation" title="Energy Conservation" class="thumbnail">
            <img src="{{ asset('/uploads/images/energy-t-4f8e841bc03af.jpg') }}" alt="Energy Conservation" />
          </a>
          <h2>
            <a href="/services/energy-conservation" title="Energy Conservation">Energy Conservation</a>
          </h2>
          <p class="description">Saving energy means saving money and saving t...
        </p>
          <a href="/services/energy-conservation" class="more">› More information</a>
        </li>
        <li class="">
          <a href="/services/property-block-management-services" title="Block Management Services and Maintenance Contracts" class="thumbnail">
            <img src="{{ asset('/uploads/images/solar-pv-t-4f8e842ae4f8e.jpg') }}" alt="Block Management Services and Maintenance Contracts" />
          </a>
          <h2>
            <a href="/services/property-block-management-services" title="Block Management Services and Maintenance Contracts">Block Management Services and Maintenance Contracts</a>
          </h2>
          <p class="description">Property Management Services...
        </p>
          <a href="/services/property-block-management-services" class="more">› More information</a>
        </li>
        <li class="">
          <a href="/services/security" title="Security" class="thumbnail">
            <img src="{{ asset('/uploads/images/security-t-4f8e843d2a755.jpg') }}" alt="Security" />
          </a>
          <h2>
            <a href="/services/security" title="Security">Security</a>
          </h2>
          <a href="/services/security" class="more">› More information</a>
        </li>
        <li class="clear"></li>
        <li class="first">
          <a href="/services/cctv" title="Closed Circuit Television CCTV" class="thumbnail">
            <img src="{{ asset('/uploads/images/cctv-t-4f8e844fc6123.jpg') }}" alt="Closed Circuit Television CCTV" />
          </a>
          <h2>
            <a href="/services/cctv" title="Closed Circuit Television CCTV">Closed Circuit Television CCTV</a>
          </h2>
          <p class="description">Our CCTV and access control systems help to p...
        </p>
          <a href="/services/cctv" class="more">› More information</a>
        </li>
        <li class="">
          <a href="/services/testing-and-inspection" title="Testing and Inspection" class="thumbnail">
            <img src="{{ asset('/uploads/images/testing-4f9fd1597fec2.jpg') }}" alt="Testing and Inspection" />
          </a>
          <h2>
            <a href="/services/testing-and-inspection" title="Testing and Inspection">Testing and Inspection</a>
          </h2>
          <p class="description">Testing your electrics could save you thousan...
        </p>
          <a href="/services/testing-and-inspection" class="more">› More information</a>
        </li>
        <li class="clear"></li>
      </ul>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
