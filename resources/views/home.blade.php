<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Electrical Services &amp; Electrical Contractors, West Yorkshire | Top Sparks</title>
    <meta name="keywords" content="electricians in bradford, electricians in leeds, electricians in yorkshire, electrical contractors, electrical electricians, electrical services" />
    <meta name="description" content="Specialist CHAS approved electrical contractors for commercial, industrial &amp; domestic electrical work in Bradford &amp; Leeds. Free no obligation quote or site visit." />
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/script.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>
  </head>
  <body id="home">
    <div class="header">
      <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="active first">
            <a class="" href="/home/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="splash">
      <div class="splash-inner container">
        <a href="javascript:;" title="Next" id="next">Next</a>
        <a href="javascript:;" title="Previous" id="prev">Previous</a>
        <ul class="splash-list cycle">
          <li>
            <a href="" title="">
              <img src="{{ asset('/uploads/images/shutterstock_57276640-4fa14817b14e2.jpg') }}" alt="" />
              <div class="caption-wrap">
                <span class="caption">
                  <span>  › click here for more information</span>
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="" title="">
              <img src="{{ asset('/uploads/images/shutterstock_48384394-4fa147ff6b7a5.jpg') }}" alt="" />
              <div class="caption-wrap">
                <span class="caption">
                  <span>  › click here for more information</span>
                </span>
              </div>
            </a>
          </li>
          <li>
            <a href="" title="">
              <img src="{{ asset('/uploads/images/shutterstock_61392457-4fa147dc26588.jpg') }}" alt="" />
              <div class="caption-wrap">
                <span class="caption">
                  <span>  › click here for more information</span>
                </span>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <div class="text home"><h1>Welcome to Top Sparks Ltd</h1><p>Top Sparks are a specialist electrical and security contractor based in the north of England. </p>
<p>We are experts in all aspects of <strong>commercial</strong>, industrial and domestic electrical work. Our clients include many of the UK's top companies and they all value our <strong>professional </strong>and <strong>personable </strong>service.  </p>
<p>We pride ourselves on being able to offer <strong>clear information</strong> and advice to clients who may not have any<strong> technical knowledge</strong>. We will always endevour to offer you the <strong>best advice</strong>, products and services in what is a<strong> </strong>fast-moving industry.</p>
<ul>
<li>Free, no obligation quote or site visit</li>
<li>Highest standards of customer service</li>
<li>Reliable and professional</li>
<li>Public liability and indemnity insurance to £2million.</li>
<li>We are proud members of <a href="http://www.napit.org.uk/member/4810/top-sparks-limited.aspx" target="_blank">NAPIT</a></li>
</ul>
<a href="/about-us" title="Read full case study" class="button yellow">› Find out more about us</a></div>
        <div class="services home">
          <h2>Services</h2>
          <ul>
            <li>
              <a href="/services/installation" class="icon">
                <img src="{{ asset('/uploads/images/installation-4f8e83fbe453a.png') }}" alt="Installation" />
              </a>
              <div class="info">
                <h3>
                  <a href="/services/installation" class="title">Installation</a>
                </h3>
                <p class="description">We carry out high quality installations in all commercial and domestic properties</p>
                <a href="/services/installation" class="more">› read more</a>
              </div>
              <span class="clear"></span>
            </li>
            <li>
              <a href="/services/testing-and-inspection" class="icon">
                <img src="{{ asset('/uploads/images/testing-4f8e846910fa0.png') }}" alt="Testing and Inspection" />
              </a>
              <div class="info">
                <h3>
                  <a href="/services/testing-and-inspection" class="title">Testing and Inspection</a>
                </h3>
                <p class="description">Testing your electrics could save you thousands in the long run. Get your unbiased report now!</p>
                <a href="/services/testing-and-inspection" class="more">› read more</a>
              </div>
              <span class="clear"></span>
            </li>
            <li>
              <a href="/services/energy-conservation" class="icon">
                <img src="{{ asset('/uploads/images/energy-conservation-4f8e841bc0ac7.png') }}" alt="Energy Conservation" />
              </a>
              <div class="info">
                <h3>
                  <a href="/services/energy-conservation" class="title">Energy Conservation</a>
                </h3>
                <p class="description">Saving energy means saving money and saving the environment</p>
                <a href="/services/energy-conservation" class="more">› read more</a>
              </div>
              <span class="clear"></span>
            </li>
            <li>
              <a href="/services/cctv" class="icon">
                <img src="{{ asset('/uploads/images/cctv-4f8e844fc681d.png') }}" alt="Closed Circuit Television CCTV" />
              </a>
              <div class="info">
                <h3>
                  <a href="/services/cctv" class="title">CCTV</a>
                </h3>
                <p class="description">Our CCTV and access control systems help to protect you and your assetts.</p>
                <a href="/services/cctv" class="more">› read more</a>
              </div>
              <span class="clear"></span>
            </li>
          </ul>
        </div>
      </div>
      <div class="column2-right">
        <div class="case-study home">
          <h2>Latest Case Study</h2>
          <h3>
            <a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project"></a>
          </h3>
          <div class="text"><a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project" class="thumbnail"><img src="{{ asset('/uploads/thumbs/photo-11-4fb7a4dddbfa8.jpg') }}" alt="Our 'grand designs' project" /></a><p>A 'grand designs' new build within the shell of a victorian mill</p>
<p> </p>
</div>
          <a href="/case-studies/our-grand-designs-project" title="Read full case study" class="button yellow">› Read full case study</a>
        </div>
        <div class="twitter_feed">
          <a class="twitter-timeline" href="https://twitter.com/top_sparks">Tweets by Top Sparks Ltd</a>
          <script src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
      </div>
      <span class="clear"></span>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/workspace/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
