
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Case Studies | Top Sparks</title>
    <meta name="keywords" content="electricians in bradford, electricians in leeds, electricians in yorkshire, electrical contractors, electrical electricians, electrical services" />
    <meta name="description" content="Specialist CHAS approved electrical contractors for commercial, industrial &amp; domestic electrical work in Bradford &amp; Leeds. Free no obligation quote or site visit." />
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>
  </head>
  <body id="case-studies">
    <div class="header">
      <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="first">
            <a class="" href="/home/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="active">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="text single"><h1>Domestic Work</h1><p><strong>Installation</strong>. We pride ourselves on carrying out all work in your home in a courteous, professional manor. As well as keeping you informed at all stages of the work, we'll make sure you're entirely happy with the finished job - and tidy up after ourselves.</p>
<p><strong>Test &amp; Inspection</strong>. Thinking of moving house? We can inspect the electrics of your potential new house and provide you with a thorough unbiased report of their condition. In particular, if the house is more than 10-15 years old we would recommend that you have the installation inspected - it could save you thousands in the long run.</p>
<p><strong>Security</strong>. We can specify and install many different types of intruder alarm and CCTV system. You might be surprised how little peace of mind will cost.</p>
<p><strong>Landlords</strong>. As a landlord you're obliged in law to ensure the property is safe for your tenants. Our landlord's inspection services ensure you're on the right side of the law and your tenants won't receive any nasty shocks. Services include Electrical Installation Condition Reports, change of tenant visual inspections, PAT testing ,maintenance contracts and quick-response fault callouts</p>
<p>Our work is inspected regularly by <a href="https://web.archive.org/web/20180902100045/http://www.napit.org.uk/">NAPIT</a> to ensure a high standards.</p>
</div>
      <div class="featured">
        <div class="description">
          <div class="text"><h2><a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project">Our 'grand designs' project</a></h2><h3><a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project"></a></h3><p>A 'grand designs' new build within the shell of a victorian mill</p>
<p>&nbsp;</p>
</div>
          <a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project" class="button">› Read full case study</a>
        </div>
        <div class="image">
          <a href="/case-studies/our-grand-designs-project" title="Our 'grand designs' project">
            <img src="{{ asset('/uploads/images/featured-case-study.png') }}" alt="featured case study" class="label">
            <img src="{{ asset('/uploads/thumbs/photo-17-4fb7a4109bbc7.jpg') }}" alt="Our 'grand designs' project">
          </a>
        </div>
      </div>
      <ul class="service-list">
        <li class="first">
          <a href="/case-studies/office-refurbishment-cat-5-and-power-installation" title="Office refurbishment - Cat 5 and power installation" class="thumbnail">
            <img src="{{ asset('/uploads/thumbs/photo-6-4fb79fbfe19d7.jpg') }}" alt="Office refurbishment - Cat 5 and power installation">
          </a>
          <div>
            <h2>
              <a href="/case-studies/office-refurbishment-cat-5-and-power-installation" title="Office refurbishment - Cat 5 and power installation">Office refurbishment - Cat 5 and power installation</a>
            </h2>
            <p class="description"><span>Installation</span>Office refurbishment - Cat 5 and power ...
        	</p>
            <a href="/case-studies/office-refurbishment-cat-5-and-power-installation" class="more">› Read Case Study</a>
          </div>
        </li>
        <li class="">
          <a href="/case-studies/shop-rewire-leeds" title="Shop rewire Leeds" class="thumbnail">
            <img src="{{ asset('/uploads/thumbs/photo-1-4fb79bd34acd7.jpg') }}" alt="Shop rewire Leeds">
          </a>
          <div>
            <h2>
              <a href="/case-studies/shop-rewire-leeds" title="Shop rewire Leeds">Shop rewire Leeds</a>
            </h2>
            <p class="description"><span>Installation</span>A full shop rewrite completed in record...
        	</p>
            <a href="/case-studies/shop-rewire-leeds" class="more">› Read Case Study</a>
          </div>
        </li>
        <li class="">
          <a href="/case-studies/intruder-alarm-installation" title="Intruder Alarm installation" class="thumbnail">
            <img src="{{ asset('/uploads/thumbs/25115_358345503007_672343007_3-4f9bfc59cf147.jpg') }}" alt="Intruder Alarm installation">
          </a>
          <div>
            <h2>
              <a href="/case-studies/intruder-alarm-installation" title="Intruder Alarm installation">Intruder Alarm installation</a>
            </h2>
            <a href="/case-studies/intruder-alarm-installation" class="more">› Read Case Study</a>
          </div>
        </li>
        <li class="clear"></li>
        <li class="first">
          <a href="/case-studies/3-phase-machine-installation" title="3 Phase Machine Installation" class="thumbnail">
            <img src="{{ asset('/uploads/thumbs/8817_128984113007_672343007_25-4f9bfb364fd60.jpg') }}" alt="3 Phase Machine Installation">
          </a>
          <div>
            <h2>
              <a href="/case-studies/3-phase-machine-installation" title="3 Phase Machine Installation">3 Phase Machine Installation</a>
            </h2>
            <p class="description"><span>Installation</span>Installation of 3 phase sub-board and K...
        	</p>
            <a href="/case-studies/3-phase-machine-installation" class="more">› Read Case Study</a>
          </div>
        </li>
        <li class="">
          <a href="/case-studies/house-re-wire-mirfield" title="House re-wire Mirfield" class="thumbnail">
            <img src="{{ asset('/uploads/thumbs/016-4f9bf46e32b6b.jpg') }}" alt="House re-wire Mirfield">
          </a>
          <div>
            <h2>
              <a href="/case-studies/house-re-wire-mirfield" title="House re-wire Mirfield">House re-wire Mirfield</a>
            </h2>
            <p class="description"><span>Installation</span>A full re-wire of a house in Mirfield...
        	</p>
            <a href="/case-studies/house-re-wire-mirfield" class="more">› Read Case Study</a>
          </div>
        </li>
        <li class="">
          <a href="/case-studies/warehouse-lighting-upgrade" title="Warehouse lighting upgrade" class="thumbnail">
            <img src="{{ asset('/uploads/thumbs/012-4f9bfcc5ee5a6.jpg') }}" alt="Warehouse lighting upgrade">
          </a>
          <div>
            <h2>
              <a href="/case-studies/warehouse-lighting-upgrade" title="Warehouse lighting upgrade">Warehouse lighting upgrade</a>
            </h2>
            <p class="description"><span>Installation</span>Installation of ware house lighting...
        	</p>
            <a href="/case-studies/warehouse-lighting-upgrade" class="more">› Read Case Study</a>
          </div>
        </li>
        <li class="clear"></li>
        <li class="clear"></li>
      </ul>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
