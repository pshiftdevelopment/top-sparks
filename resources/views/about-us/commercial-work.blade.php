
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Commercial Installation | Top Sparks</title>
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>

  </head>
  <body id="about-us">
    <div class="header">
     <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="first">
            <a class="" href="/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown active">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <div class="text main"><h1>Commercial Work</h1><p>From new installations to ongoing maintenance, we can deal with all aspects of electrical work within your business. Our aim is to provide the complete no-hassle service.&nbsp;</p>
<p>Power, lighting and security - we can take care of it all!</p>
<p>Our commercial services include:</p>
<ul>
<li>Commercial building testing and Inspection for insurance requirements</li>
<li>Maintenance contracts.</li>
<li>Fire alarm design,installation and maintenance.</li>
<li>Emergency lighting design installation and maintenance.</li>
<li>Lighting design, installation and maintenance</li>
<li>3-phase installation</li>
<li>CCTV and Intruder alarms</li>
<li>Access Control</li>
<li>Covert CCTV Installation for employee theft problems</li>
<li>Energy saving lighting systems</li>
</ul>
</div>
      </div>
      <div class="column2-right">
        <a title="" rel="gallery" class="modal cboxElement" href="{{ asset('/uploads/images/img_0075-4fb7a9cd63d21.jpg') }}">
          <img src="{{ asset('/uploads/thumbs/img_0075-4fb7a9cd63d21.jpg') }}" alt="">
        </a>
      </div>
      <span class="clear"></span>
    </div>

    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
