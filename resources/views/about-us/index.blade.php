
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>About Us | Top Sparks</title>
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>

  </head>
  <body id="about-us">
    <div class="header">
     <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="first">
            <a class="" href="/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown active">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <div class="text main"><h1>About Top Sparks Ltd</h1><p>Top Sparks was started by managing director Nick Scanlon BSc (Hons) MIET, who has over 25 years experience in the electrical and security installation industry. His principle aim was to offer a professional service with the the highest standards of customer service - something which other companies consistantly fail to deliver.</p>
<p>This approach has paid off, with the business going from strength to strength and the vast majority of new business coming via recommendation and word of mouth.</p>
<h2>
	Knowledge &amp; experience:</h2>
<ul>
<li>City &amp; Guids 2382 17th Edition Wiring Regulations certificate</li>
<li>City &amp; Guilds 2391 Inspection, Testing and Certification of Electrical Installations</li>
<li>City &amp; Guilds 2381 16th Edition Wiring Regulations certificate</li>
<li>City &amp; Guilds 2360 Electrical Installation Part1 and Part 2</li>
<li>City &amp; Guilds 1850 Fire, Security and Emergency Alarm Systems Part1 and Part 2</li>
<li>City &amp; Guilds 2372 Installing and Testing Photovoltaic Systems (Solar Panels)</li>
<li>Leeds College of Technology 2-day PAT testing course certificate.</li>
<li>Diploma in IT and Computing.</li>
<li>Member of the Institute of Electrical Engineers (MIEE)</li>
<li>Member of the Institute of Engineering and Technology (MIET)</li>
<li>BSc (Hons) degree.</li>
<li>CHAS registered for Health &amp; Safety</li>
<li>IPAF qualified for boom access equipment</li>
</ul>
<h2>
	We Care</h2>
<p>When working in your home or business, we'll treat it with respect at all times. We always leave the work site clean and tidy.</p>
<h2>
	Reliability</h2>
<p>There's nothing worse than tradesmen or contractors who don't turn up on time. Or simply don't turn up. We don't do that. If we<br>
say we'll be there - we'll be there!</p>
<h2>
	Peace of Mind</h2>
<p>We hold public liability and indemnity insurance to £2million.</p>
</div>
      </div>
      <div class="column2-right">
        <a title="" rel="gallery" class="modal cboxElement" href="{{ asset('/uploads/images/293669_10150273528958008_31859-4fb7ab9c83dc9.jpg') }}">
          <img src="{{ asset('/uploads/thumbs/293669_10150273528958008_31859-4fb7ab9c83dc9.jpg') }}" alt="">
        </a>
        <a title="van" rel="gallery" class="modal cboxElement" href="{{ asset('/uploads/images/fullsizerender-5-59b50593028c5.jpg') }}">
          <img src="{{ asset('/uploads/thumbs/fullsizerender-5-59b50593028c5.jpg') }}" alt="van">
        </a>
      </div>
      <span class="clear"></span>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
