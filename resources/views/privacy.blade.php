
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Privacy Policy| Top Sparks</title>
    <meta name="keywords" content="electricians in bradford, electricians in leeds, electricians in yorkshire, electrical contractors, electrical electricians, electrical services" />
    <meta name="description" content="Specialist CHAS approved electrical contractors for commercial, industrial &amp; domestic electrical work in Bradford &amp; Leeds. Free no obligation quote or site visit." />
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/assets/css/style.css') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('/assets/js/jquery.cycle.lite.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/jquery.colorbox.min.js') }}" type="text/javascript"></script>
    <script src="/assets/js/script.js" type="text/javascript"></script>
    <script type="text/javascript">
			$(document).ready(function(){
				$('.modal').colorbox();
				$('.cycle').cycle({
					next: '#next',
					prev: '#prev'
				});
				$('#side-case-studies li:not(.active) ul').hide();
				$('#side-case-studies li span').on('click', function(){
					if(!$(this).parent('li').is('.active'))
					{
						$(this).next('ul').slideDown(150);
						$(this).parent('li').addClass('active');
					}
					else
					{
						$(this).next('ul').hide();
						$(this).parent('li').removeClass('active');
					}
				})
				$('.thumbnails a').on('click', function(e){
					e.preventDefault();
					if(!$(this).parent('li').is('.active'))
					{
						var alt = $(this).attr('title');
						var imgURL = $(this).attr('href');
						var img = $('<img src="'+imgURL+'" alt="'+alt+'" />');
						$('.gallery').prepend(img);
						$('.gallery img:eq(1)').fadeOut(350);
						setTimeout(function(){ $('.gallery img:eq(1)').remove() }, 400);
						$('.thumbnails li').removeClass('active');
						$(this).parent('li').addClass('active');
					}
				})
			})
		</script>
  </head>
  <body id="privacy">
    <div class="header">
      <div class="container">
        <a href="" title="Top Sparks">
          <img src="{{ asset('/assets/images/logo.png') }}" alt="Top Sparks" class="header-logo" />
        </a>
        <div class="header-contact">
          <p><span>Call:</span> 01924 491362  or <span>E-mail:</span> info@topsparks.co.uk</p>
        </div>
        <div id="resi-nav"></div>
        <ul class="navigation" id="top">
          <li class="active first">
            <a class="" href="/home/">
              <span>Home</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/services/">
              <span>Services</span>
            </a>
            <ul class="dropdown">
              <li class="desktop-hide">
                <a href="/services">Services</a>
              </li>
              <li class="first">
                <a href="/services/design/">Design</a>
              </li>
              <li>
                <a href="/services/installation/">Installation</a>
              </li>
              <li>
                <a href="/services/maintenance/">Maintenance</a>
              </li>
              <li>
                <a href="/services/energy-conservation/">Energy Conservation</a>
              </li>
              <li>
                <a href="/services/property-block-management-services/">Property/ Block Management Services</a>
              </li>
              <li>
                <a href="/services/security/">Security</a>
              </li>
              <li>
                <a href="/services/cctv/">CCTV</a>
              </li>
              <li>
                <a href="/services/testing-and-inspection/">Testing and Inspection</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/case-studies/">
              <span>Case Studies</span>
            </a>
          </li>
          <li class="dropdown">
            <a class="drop-trig" href="/about-us/">
              <span>About Us</span>
            </a>
            <ul class="dropdown">
              <li class="first">
                <a href="/about-us/domestic-work/">Domestic Work</a>
              </li>
              <li>
                <a href="/about-us/commercial-work/">Commercial Work</a>
              </li>
              <li>
                <a href="/about-us/about-us/">About Us</a>
              </li>
            </ul>
          </li>
          <li class="">
            <a class="" href="/contact-us/">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="content container">
      <div class="column2-left">
        <div class="text main"><h1>Privacy Policy</h1><p>Privacy Policy</p>
<p>What is this Privacy Policy for?<br>
This privacy policy is for this website [www.topsparks.co.uk] and served by Top Sparks Ltd and governs the privacy of its users who choose to use it.</p>
<p>The policy sets out the different areas where user privacy is concerned and outlines the obligations &amp; requirements of the users, the website and website owners. Furthermore the way this website processes, stores and protects user data and information will also be detailed within this policy.</p>
<p>The Website<br>
This website and its owners take a proactive approach to user privacy and ensure the necessary steps are taken to protect the privacy of its users throughout their visiting experience. This website complies to all UK national laws and requirements for user privacy.</p>
<p>Use of Cookies<br>
This website uses cookies to better the users experience while visiting the website. Where applicable this website uses a cookie control system allowing the user on their first visit to the website to allow or disallow the use of cookies on their computer / device. This complies with recent legislation requirements for websites to obtain explicit consent from users before leaving behind or reading files such as cookies on a user's computer / device.</p>
<p>Cookies are small files saved to the user's computers hard drive that track, save and store information about the user's interactions and usage of the website. This allows the website, through its server to provide the users with a tailored experience within this website.<br>
Users are advised that if they wish to deny the use and saving of cookies from this website on to their computers hard drive they should take necessary steps within their web browsers security settings to block all cookies from this website and its external serving vendors.</p>
<p>This website uses tracking software to monitor its visitors to better understand how they use it. This software is provided by Google Analytics which uses cookies to track visitor usage. The software will save a cookie to your computers hard drive in order to track and monitor your engagement and usage of the website, but will not store, save or collect personal information. You can read Google's privacy policy here for further information [ http://www.google.com/privacy.html ].<br>
Other cookies may be stored to your computers hard drive by external vendors when this website uses referral programs, sponsored links or adverts. Such cookies are used for conversion and referral tracking and typically expire after 30 days, though some may take longer. No personal information is stored, saved or collected.</p>
<p>Contact &amp; Communication<br>
Users contacting this website and/or its owners do so at their own discretion and provide any such personal details requested at their own risk. Your personal information is kept private and stored securely until a time it is no longer required or has no use, as detailed in the Data Protection Act 1998. Every effort has been made to ensure a safe and secure form to email submission process but advise users using such form to email processes that they do so at their own risk.<br>
This website and its owners use any information submitted to provide you with further information about the products / services they offer or to assist you in answering any questions or queries you may have submitted. This includes using your details to subscribe you to any email newsletter program the website operates but only if this was made clear to you and your express permission was granted when submitting any form to email process. Or whereby you the consumer have previously purchased from or enquired about purchasing from the company a product or service that the email newsletter relates to. This is by no means an entire list of your user rights in regard to receiving email marketing material. Your details are not passed on to any third parties..</p>
<p>External Links</p>
<p>Although this website only looks to include quality, safe and relevant external links, users are advised adopt a policy of caution before clicking any external web links mentioned throughout this website.</p>
<p>The owners of this website cannot guarantee or verify the contents of any externally linked website despite their best efforts. Users should therefore note they click on external links at their own risk and this website and its owners cannot be held liable for any damages or implications caused by visiting any external links mentioned.</p>
<p>Social Media Platforms</p>
<p>Communication, engagement and actions taken through external social media platforms that this website and its owners participate on are custom to the terms and conditions as well as the privacy policies held with each social media platform respectively.</p>
<p>Users are advised to use social media platforms wisely and communicate / engage upon them with due care and caution in regard to their own privacy and personal details. This website nor its owners will ever ask for personal or sensitive information through social media platforms and encourage users wishing to discuss sensitive details to contact them through primary communication channels such as by telephone or email.</p>
<p>This website may use social sharing buttons which help share web content directly from web pages to the social media platform in question. Users are advised before using such social sharing buttons that they do so at their own discretion and note that the social media platform may track and save your request to share a web page respectively through your social media platform account.</p>
<p>Shortened Links in Social Media<br>
This website and its owners through their social media platform accounts may share web links to relevant web pages. By default some social media platforms shorten lengthy urls [web addresses] (this is an example: http://bit.ly/zyVUBo).<br>
Users are advised to take caution and good judgement before clicking any shortened urls published on social media platforms by this website and its owners. Despite the best efforts to ensure only genuine urls are published many social media platforms are prone to spam and hacking and therefore this website and its owners cannot be held liable for any damages or implications caused by visiting any shortened links.</p>
<p>Resources &amp; Further Information<br>
Data Protection Act 1998<br>
Privacy and Electronic Communications Regulations 2003<br>
Privacy and Electronic Communications Regulations 2003 - The Guide<br>
Twitter Privacy Policy<br>
Facebook Privacy Policy<br>
Google Privacy Policy<br>
Linkedin Privacy Policy<br>
Mailchimp Privacy Policy<br>
Fillable PDF Forms Creation<br>
Website Privacy Policy Template<br>
&nbsp;</p>
</div>
      </div>
      <span class="clear"></span>
    </div>
    <div class="footer">
      <div class="footer-inner container">
        <p class="info"><strong>Copyright © Top Sparks Ltd 2017</strong><br />
                    All rights reserved worldwide<br /><a href="/terms-and-conditions" title="Terms &amp; conditions">Terms</a> | <a href="/privacy" title="Privacy Policy">Privacy Policy</a></p>
        <img src="{{ asset('/assets/images/logos.png') }}" class="logos" alt="logos" />
      </div>
    </div>
    <div class="cookie-notice">
      <p>We use cookies on this website, by continuing to browse the site you are agreeing to our use of cookies.  <a href="/privacy">Find out more</a>.</p>
      <div class="c-agree"></div>
    </div>
  </body>
</html>
